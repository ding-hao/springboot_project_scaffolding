package com.dh.modules.system.entity.vo;

import com.dh.modules.system.entity.Admin;
import io.swagger.annotations.ApiModelProperty;

public class AdminVO extends Admin {

    @ApiModelProperty(value = "后台用户id", required = true)
    private String id;

    @ApiModelProperty(value = "后台用户账户", required = true)
    private String username;

    @ApiModelProperty(value = "后台用户密码", required = true)
    private String password;

    @ApiModelProperty(value = "后台用户图标", required = true)
    private String avatar;

    @ApiModelProperty(value = "后台用户是否有效", required = true)
    private Boolean deleted;

    @ApiModelProperty(value = "后台用户等级", required = true)
    private Integer version;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    @Override
    public String toString() {
        return "AdminVO{" +
                "id='" + id + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", avatar='" + avatar + '\'' +
                ", deleted=" + deleted +
                ", version=" + version +
                '}';
    }
}
