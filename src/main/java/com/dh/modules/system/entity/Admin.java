package com.dh.modules.system.entity;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@Entity
@Table(name = "admin")
@ApiModel("后台登录用户信息")
public class Admin {

	@Id
	@ApiModelProperty(value = "后台用户id", required = true, hidden = true)
    @GenericGenerator(name = "system-uuid", strategy = "uuid2")
    @GeneratedValue(generator = "system-uuid")
    private String id;

	@ApiModelProperty(value = "后台用户账户", required = true)
    private String username;

	@ApiModelProperty(value = "后台用户密码", required = true)
    private String password;

	@ApiModelProperty(value = "后台用户ip", required = true, hidden = true)
    private String lastLoginIp;

	@ApiModelProperty(value = "后台用户登录的时间", required = true, hidden = true)
    private LocalDateTime lastLoginTime;

	@ApiModelProperty(value = "后台用户图标", required = true)
    private String avatar;

	@ApiModelProperty(value = "后台用户添加时间", required = true, hidden = true)
    private LocalDateTime addTime;

	@ApiModelProperty(value = "后台用户是否有效", required = true, hidden = true)
    private Boolean deleted;

	@ApiModelProperty(value = "后台用户等级", required = true)
    private Integer version;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getLastLoginIp() {
		return lastLoginIp;
	}

	public void setLastLoginIp(String lastLoginIp) {
		this.lastLoginIp = lastLoginIp;
	}

	public LocalDateTime getLastLoginTime() {
		return lastLoginTime;
	}

	public void setLastLoginTime(LocalDateTime lastLoginTime) {
		this.lastLoginTime = lastLoginTime;
	}

	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	public LocalDateTime getAddTime() {
		return addTime;
	}

	public void setAddTime(LocalDateTime addTime) {
		this.addTime = addTime;
	}

	public Boolean getDeleted() {
		return deleted;
	}

	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}

	public Integer getVersion() {
		return version;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}


	@Override
	public String toString() {
		return "Admin [id=" + id + ", username=" + username + ", password=" + password + ", lastLoginIp=" + lastLoginIp
				+ ", lastLoginTime=" + lastLoginTime + ", avatar=" + avatar + ", addTime=" + addTime + ", deleted="
				+ deleted + ", version=" + version + "]";
	}


}
