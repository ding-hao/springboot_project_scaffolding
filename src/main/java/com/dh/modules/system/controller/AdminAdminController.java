package com.dh.modules.system.controller;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.NotNull;

import cn.hutool.core.util.ReUtil;
import com.dh.modules.system.entity.vo.AdminVO;
import com.dh.modules.system.service.AdminService;
import com.dh.modules.system.service.AdminTokenManager;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.dh.common.auth.annotation.LoginAdmin;
import com.dh.modules.system.entity.Admin;
import com.dh.common.utils.ResponseUtil;
import com.dh.common.utils.bcrypt.BCryptPasswordEncoder;
import com.dh.common.validator.Order;
import com.dh.common.validator.Sort;

@RestController
@RequestMapping("/admin/admin")
@Api(tags = "管理员信息")
@Validated
public class AdminAdminController {

    @Autowired
    private AdminService adminService;

    @GetMapping("/info")
    public Object info(String token) {
        String adminId = AdminTokenManager.getUserId(token);
        if (adminId == null) {
            return ResponseUtil.badArgumentValue();
        }
        Admin admin = adminService.findById(adminId);
        if (admin == null) {
            return ResponseUtil.badArgumentValue();
        }

        Map<String, Object> data = new HashMap<>();
        data.put("name", admin.getUsername());
        data.put("avatar", admin.getAvatar());

        // 目前roles不支持，这里简单设置admin
        List<String> roles = new ArrayList<>();
        roles.add("admin");
        data.put("roles", roles);
        data.put("introduction", "admin introduction");
        return ResponseUtil.ok(data);
    }

    @GetMapping("/list")
//	@ApiImplicitParams({
//			@ApiImplicitParam(name = "adminId", value = "自定义参数解析器赋值", dataType = "string", paramType = "query"), })
    public Object list(@ApiParam(hidden = true) @LoginAdmin String adminId, String username, @RequestParam(defaultValue = "1") Integer page,
                       @RequestParam(defaultValue = "10") Integer pagesize,
                       @Sort @RequestParam(defaultValue = "addTime") String sort,
                       @Order @RequestParam(defaultValue = "desc") String order) {
        if (adminId == null) {
            return ResponseUtil.unlogin();
        }
        // 原方法优化
        // List<Admin> adminList = adminService.querySelective(username, page, limit,
        // sort, order);
        // int total = adminService.countSelective(username, page, limit, sort, order);
        page = page - 1 >= 0 ? page - 1 : 0;
        Page<Admin> allAdmin = adminService.selectAllAdmin(username, page, pagesize, sort, order);
        List<Admin> adminList = allAdmin.getContent();
        int total = allAdmin.getSize();
        Map<String, Object> data = new HashMap<>();
        data.put("total", total);
        data.put("items", adminList);

        return ResponseUtil.ok(data);
    }

    private Object validate(Admin admin) {
        String name = admin.getUsername();
        if (StringUtils.isEmpty(name)) {
            return ResponseUtil.badArgument();
        }
        if (ReUtil.isMatch("^[\\w\\u4e00-\\u9fa5]{6,20}(?<!_)$", name)) {
            return ResponseUtil.fail(402, "管理员名称不符合规定");
        }
        String password = admin.getPassword();
        if (StringUtils.isEmpty(password) || password.length() < 6) {
            return ResponseUtil.fail(402, "管理员密码长度不能小于6");
        }
        return null;
    }


    @PostMapping("/create")
    public Object create(@LoginAdmin @ApiParam(hidden = true) String adminId, @RequestBody Admin admin) {
        if (adminId == null) {
            return ResponseUtil.unlogin();
        }
        Object error = validate(admin);
        if (error != null) {
            return error;
        }

        String username = admin.getUsername();
        List<Admin> adminList = adminService.findAdmin(username);
        if (adminList.size() > 0) {
            return ResponseUtil.fail(402, "管理员已经存在");
        }

        String rawPassword = admin.getPassword();
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        String encodedPassword = encoder.encode(rawPassword);
        admin.setPassword(encodedPassword);
        admin.setAddTime(LocalDateTime.now());
        admin.setDeleted(false);

        adminService.add(admin);
        return ResponseUtil.ok(admin);
    }

    @GetMapping("/read")
    public Object read(@ApiParam(hidden = true) @LoginAdmin String adminId, @NotNull String id) {
        if (adminId == null) {
            return ResponseUtil.unlogin();
        }

        Admin admin = adminService.findById(id);
        return ResponseUtil.ok(admin);
    }

    @PostMapping("/update")
    public Object update(@ApiParam(hidden = true) @LoginAdmin String adminId, @RequestBody AdminVO admin) {
        if (adminId == null) {
            return ResponseUtil.unlogin();
        }
        Object error = validate(admin);
        if (error != null) {
            return error;
        }

        String anotherAdminId = admin.getId();
        if (anotherAdminId == null) {
            return ResponseUtil.badArgument();
        }
        // TODO 这里开发者需要删除以下检验代码
        // 目前这里不允许修改超级管理员是防止演示平台上他人修改管理员密码而导致登录失败
        if (anotherAdminId == "1") {
            return ResponseUtil.fail(403, "超级管理员不能修改");
        }

        String rawPassword = admin.getPassword();
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        String encodedPassword = encoder.encode(rawPassword);
        admin.setPassword(encodedPassword);

        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = attributes.getRequest();
        admin.setLastLoginIp(request.getRemoteAddr());

        if (adminService.updateById(admin) == 0) {
            return ResponseUtil.updatedDateExpired();
        }

        return ResponseUtil.ok(admin);
    }

    @PostMapping("/delete")
    public Object delete(@ApiParam(hidden = true) @LoginAdmin String adminId, @RequestBody AdminVO admin) {
        if (adminId == null) {
            return ResponseUtil.unlogin();
        }

        String anotherAdminId = admin.getId();
        if (anotherAdminId == null) {
            return ResponseUtil.badArgument();
        }
        // TODO 这里开发者需要删除以下检验代码
        // 目前这里不允许删除超级管理员是防止演示平台上他人删除管理员账号而导致登录失败
        if (anotherAdminId == "1") {
            return ResponseUtil.fail(403, "超级管理员不能删除");
        }

        adminService.deleteById(anotherAdminId);
        return ResponseUtil.ok();
    }
}
