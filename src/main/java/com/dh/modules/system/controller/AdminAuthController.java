package com.dh.modules.system.controller;

import java.time.LocalDateTime;
import java.util.List;

import com.dh.modules.system.entity.vo.LoginVO;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dh.common.auth.annotation.LoginAdmin;
import com.dh.modules.system.dao.AdminToken;
import com.dh.modules.system.entity.Admin;
import com.dh.modules.system.service.AdminService;
import com.dh.modules.system.service.AdminTokenManager;
import com.dh.common.utils.ResponseUtil;
import com.dh.common.utils.bcrypt.BCryptPasswordEncoder;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;

@RestController
@RequestMapping("/admin/login")
@Api(tags = "登录/登出")
@Validated
public class AdminAuthController {
    private final Log logger = LogFactory.getLog(AdminAuthController.class);

    @Autowired
    private AdminService adminService;

    /*
     *  { username : value, password : value }
     */
    @PostMapping("/login")
    public Object login(@RequestBody LoginVO loginVO){
        String username = loginVO.getUsername();
        String password = loginVO.getPassword();

        if(StringUtils.isEmpty(username) || StringUtils.isEmpty(password)){
            return ResponseUtil.badArgument();
        }

        List<Admin> adminList = adminService.findAdmin(username);
        Assert.state(adminList.size() < 2, "同一个用户名存在两个账户");
        if(adminList.size() == 0){
            return ResponseUtil.badArgumentValue();
        }
        Admin admin = adminList.get(0);

        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        if(!encoder.matches(password, admin.getPassword())){
        	logger.error("账号密码不对");
            return ResponseUtil.fail(403, "账号密码不对");
        }

        String adminId = admin.getId();
        // token
        AdminToken adminToken = AdminTokenManager.generateToken(adminId);
        admin.setLastLoginTime(LocalDateTime.now());
        adminService.updateById(admin);
        return ResponseUtil.ok(adminToken.getToken());
    }

    /*
     *没有清除AdminTokenManager中tokenMap,idMap保存的退出用户的信息
     */
    @PostMapping("/logout")
//    @ApiImplicitParams({
//		@ApiImplicitParam(name = "adminId", value = "自定义参数解析器赋值", dataType = "string", paramType = "query")})
    public Object logout(@LoginAdmin String adminId){
        if(adminId == null){
            return ResponseUtil.unlogin();
        }
//      清除AdminTokenManager中tokenMap,idMap和tokenMap保存的退出用户的信息
        if(AdminTokenManager.removeIdToken(adminId)) {
        	return ResponseUtil.ok();
        }
        return ResponseUtil.unlogin();
    }
}
