package com.dh.modules.system.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.dh.modules.system.entity.Admin;

public interface AdminDao extends JpaRepository<Admin, String>, JpaSpecificationExecutor<Admin> {

	/**
	 *
	* @Title: findByUsername
	* @author
	* @Description: 按用户账户查询用户信息
	* @param username
	* @return
	* @return List<Admin>
	 */
	List<Admin> findByUsername(String username);

	Admin findOne(String adminId);

}
