package com.dh.modules.system.service;

import java.util.List;

import org.springframework.data.domain.Page;

import com.dh.modules.system.entity.Admin;

public interface AdminService {

	public List<Admin> findAdmin(String username);

	public Admin findById(String adminId);

	public void deleteById(String anotherAdminId);

	public int updateById(Admin admin);

	public void add(Admin admin);

	public int countSelective(String username, Integer page, Integer pagesize, String sort, String order);

	public List<Admin> querySelective(String username, Integer page, Integer pagesize, String sort, String order);

	public Page<Admin> selectAllAdmin(String username, Integer page, Integer pagesize, String sort, String order);

}
