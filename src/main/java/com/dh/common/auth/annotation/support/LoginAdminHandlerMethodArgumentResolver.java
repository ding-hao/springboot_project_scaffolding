package com.dh.common.auth.annotation.support;

import com.dh.common.auth.annotation.LoginAdmin;
import com.dh.modules.system.service.AdminTokenManager;
import org.springframework.core.MethodParameter;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;


public class LoginAdminHandlerMethodArgumentResolver implements HandlerMethodArgumentResolver {
	public static final String LOGIN_TOKEN_KEY = "Admin-Token";

	/**
	 * 解析器是否支持当前参数
	 */
	@Override
	public boolean supportsParameter(MethodParameter parameter) {
		// 指定参数如果被应用LoginAdmin注解，则使用该解析器。
		// 如果直接返回true，则代表将此解析器用于所有参数
		return parameter.getParameterType().isAssignableFrom(String.class)
				&& parameter.hasParameterAnnotation(LoginAdmin.class);
	}

	/**
	 * 将request中的请求参数解析到当前Controller参数上
	 *
	 * @param parameter
	 *            需要被解析的Controller参数，此参数必须首先传给{@link #supportsParameter}并返回true
	 * @param container mavContainer
	 *            当前request的ModelAndViewContainer
	 * @param request webRequest
	 *            当前request
	 * @param factory binderFactory
	 *            生成实例的工厂
	 * @return 解析后的Controller参数
     *
     * @throws Exception
	 */
	@Override
	public Object resolveArgument(MethodParameter parameter, ModelAndViewContainer container, NativeWebRequest request,
			WebDataBinderFactory factory) throws Exception {

//		return new String("1");
		String token = request.getHeader(LOGIN_TOKEN_KEY);//VUE设置的请求头
		if (token == null || token.isEmpty()) {
			return null;
		}

		return AdminTokenManager.getUserId(token);
	}
}
