package com.dh.common.auth.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 *
* @ClassName: LoginAdmin
* @Description: 为指定参数用参数解析器赋值
* @author dinghao
* @date 2018年10月29日 下午3:53:24
*
 */
@Target(ElementType.PARAMETER)
@Retention(RetentionPolicy.RUNTIME)
public @interface LoginAdmin {

}
