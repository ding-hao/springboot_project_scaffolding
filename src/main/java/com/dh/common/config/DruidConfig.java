package com.dh.common.config;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import com.alibaba.druid.pool.DruidDataSource;
import com.alibaba.druid.support.http.StatViewServlet;
import com.alibaba.druid.support.http.WebStatFilter;

/**
 *
 * @author dinghao
 *
 */
@Configuration
@EnableConfigurationProperties(DruidPropertityConfig.class)
public class DruidConfig {

	// 日志
	private final Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	private DruidPropertityConfig propertityConfig;

	/**
	 * druid属性配置 Springboot 默认使用org.apache.tomcat.jdbc.pool.DataSource数据源，默认配置如下：
	 * Springboot默认支持4种数据源类型，定义在
	 * org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration
	 * 中，分别是： org.apache.tomcat.jdbc.pool.DataSource
	 * com.zaxxer.hikari.HikariDataSource org.apache.commons.dbcp.BasicDataSource
	 * org.apache.commons.dbcp2.BasicDataSource 对于这4种数据源，当 classpath
	 * 下有相应的类存在时，Springboot 会通过自动配置为其生成DataSource Bean，DataSource
	 * Bean默认只会生成一个，四种数据源类型的生效先后顺序如下：Tomcat--> Hikari --> Dbcp --> Dbcp2 。
	 * @return
	 */
	@Bean(name = "dataSource")
	@Primary
	public DataSource druidDataSource() {

		logger.info("dataSource propertityConfig:{}", propertityConfig);

		DruidDataSource dataSource = new DruidDataSource();
		dataSource.setUrl(propertityConfig.getUrl());
		dataSource.setUsername(propertityConfig.getUsername());
		dataSource.setPassword(propertityConfig.getPassword());
		dataSource.setDriverClassName(propertityConfig.getDriverClassName());
		// configuration
		dataSource.setInitialSize(propertityConfig.getInitialSize());
		dataSource.setMinIdle(propertityConfig.getMinIdle());
		dataSource.setMaxActive(propertityConfig.getMaxActive());
		dataSource.setMaxWait(propertityConfig.getMaxWait());
		dataSource.setTimeBetweenEvictionRunsMillis(propertityConfig.getTimeBetweenEvictionRunsMillis());
		dataSource.setMinEvictableIdleTimeMillis(propertityConfig.getMinEvictableIdleTimeMillis());
		dataSource.setTestWhileIdle(propertityConfig.isTestWhileIdle());
		dataSource.setTestOnBorrow(propertityConfig.isTestOnBorrow());
		dataSource.setTestOnReturn(propertityConfig.isTestOnReturn());
		dataSource.setPoolPreparedStatements(propertityConfig.isPoolPreparedStatements());
		dataSource.setMaxPoolPreparedStatementPerConnectionSize(
				propertityConfig.getMaxPoolPreparedStatementPerConnectionSize());
		return dataSource;
	}

	/**
	 * 注册一个druidStatViewServlet
	 *
	 * @return
	 */
	@Bean
	public ServletRegistrationBean druidStatViewServlet() {
		ServletRegistrationBean servletRegistrationBean = new ServletRegistrationBean(new StatViewServlet(),
				"/druid/*");
		servletRegistrationBean.addInitParameter("allow", "127.0.0.1");
		servletRegistrationBean.addInitParameter("loginUsername", "admin");
		servletRegistrationBean.addInitParameter("loginPassword", "password");
		servletRegistrationBean.addInitParameter("resetEnable", "false");
		return servletRegistrationBean;
	}

	/**
	 * 注册一个druidStatFilter
	 *
	 * @return
	 */
	@Bean
	public FilterRegistrationBean druidStatFilter() {
		FilterRegistrationBean filterRegistrationBean = new FilterRegistrationBean(new WebStatFilter());
		filterRegistrationBean.addUrlPatterns("/*");
		filterRegistrationBean.addInitParameter("exclusions", "*.js,*.gif,*.jpg,*.png,*.css,*.ico,/druid/*");
		return filterRegistrationBean;
	}

}
