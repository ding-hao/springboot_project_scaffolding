package com.dh.common.config;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.alibaba.fastjson.serializer.SerializerFeature;
import com.alibaba.fastjson.support.config.FastJsonConfig;
import com.alibaba.fastjson.support.spring.FastJsonHttpMessageConverter;


@Configuration
public class FastjsonConfig extends WebMvcConfigurerAdapter {

	// 日志
	private final Logger logger = LoggerFactory.getLogger(getClass());

	/**
	 * 利用fastjson替换掉jackson，且解决中文乱码问题
	 *
	 * @param converters
	 */
	// 方法一：extends WebMvcConfigurerAdapter
	@Override
	public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
		super.configureMessageConverters(converters);
		// 核心代码

		/*
		 * 1、先定义一个convert转换消息的对象 2、添加fastjson的配置信息，比如是否要格式化返回的json数据； 3、在convert中添加配置信息
		 * 4、将convert添加到converters
		 */

		// 1、先定义一个convert转换消息的对象
		FastJsonHttpMessageConverter fastConverter = new FastJsonHttpMessageConverter();
		// 2、添加fastjson的配置信息，比如是否要格式化返回的json数据；
		FastJsonConfig fastJsonConfig = new FastJsonConfig();
		fastJsonConfig.setSerializerFeatures(
				// 是否需要格式化
				SerializerFeature.PrettyFormat);
		// 附加：处理中文乱码（后期添加）
		List<MediaType> fastMedisTypes = new ArrayList<MediaType>();
		fastMedisTypes.add(MediaType.APPLICATION_JSON_UTF8);
		fastConverter.setSupportedMediaTypes(fastMedisTypes);
		// 3、在convert中添加配置信息
		fastConverter.setFastJsonConfig(fastJsonConfig);
		// 4、将convert添加到converters
		converters.add(fastConverter);
		logger.info("利用fastjson替换掉jackson，且解决中文乱码问题");
	}

}
