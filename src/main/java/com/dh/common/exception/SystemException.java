package com.dh.common.exception;


import com.dh.common.res.enums.SystemRes;

/**
 * 自定义的SystemException
 * @author dinghao
 *
 */
public class SystemException extends RuntimeException{

	private static final long serialVersionUID = 1L;

	private String code;

	public SystemException() {
		super();
	}

	public SystemException(SystemRes sRest) {
		super(sRest.getMsg());
		this.code = sRest.getCode();
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

}
