package com.dh.common.res;

import com.dh.common.res.enums.SystemRes;

public class ResVo <T> {

	private String status;

	private T message;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public T getMessage() {
		return message;
	}

	public void setMessage(T message) {
		this.message = message;
	}

	public ResVo() {
		super();
	}

	public ResVo(String status, T message) {
		super();
		this.status = status;
		this.message = message;
	}


	public static ResVo<Object> success(Object message) {
		return new ResVo<Object>("200",message);
	}

	public static ResVo<Object> fail(Object message) {
		return new ResVo<Object>("500",message);
	}

	public static ResVo<Object> defaultSuccess() {
		return new ResVo<Object>("200",SystemRes.getMsgByCode("200"));
	}

	public static ResVo<Object> defaultFail() {
		return new ResVo<Object>("500",SystemRes.getMsgByCode("500"));
	}

	public static ResVo<Object> unKnowError(SystemRes sRest) {
		return new ResVo<Object>(sRest.getCode(),sRest.getMsg());
	}

	public static ResVo<Object> systemError(String code,String msg) {
		return new ResVo<Object>(code,msg);
	}


	@Override
	public String toString() {
		return "ResVo [status=" + status + ", message=" + message + "]";
	}
}
