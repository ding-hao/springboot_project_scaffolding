package com.dh.common.res.enums;

import org.apache.commons.lang3.StringUtils;

public enum SystemRes {

    DEFAULT_SUCCESS("200", "操作成功"),

    DEFAULT_FAIL("500", "操作失败"),

	UNKONW_ERROR("505","未知异常");


    private String msg;

    private String code;

    public String getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }
    SystemRes(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public static String getMsgByCode(String code) {
        for (SystemRes systemException : SystemRes.values()) {
            if (StringUtils.equals(systemException.getCode(), code)) {
                return systemException.getMsg();
            }
        }
        return null;
    }


}
